import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import gumbel_r
from scipy.stats import genpareto
import seaborn as sns
import wave_number

plt.style.use('./custom_latex_style.mplstyle')
sns.set_style('whitegrid')

file = "NORA10_5674N_0501E.txt"

df = pd.read_fwf(file, skiprows=3)

hs_col_label = df.columns[16]  
tp_col_label = df.columns[17] 
year_col_label = df.columns[0] 

hs = df[hs_col_label]
tp = df[tp_col_label]
year = df[year_col_label]
year_arr = np.array(year)
num_years = int(year_arr[-1]) - int(year_arr[0])

max_tp = 8.4

max_tp_rows = df[df[tp_col_label] == max_tp]

max_hs = max_tp_rows[hs_col_label].max()

print(max_hs)

def morison_eq(rho, g, C_M, D, C_D, H, T, d):
    omega = 2 * np.pi / T

    def dispersion_relation(k):
        return omega**2 - g * k * np.tanh(k * d)

    def dispersion_relation_derivative(k):
        return -g * d * (1/np.cosh(k * d))**2 + g * d * k * (np.tanh(k * d))**2
    
    def find_wavenumber():
        k_guess = omega**2 / g  # initial guess
        max_iterations = 200
        tolerance = 1e-4

        for i in range(max_iterations):
            k_next = k_guess - dispersion_relation(k_guess) / dispersion_relation_derivative(k_guess)
            if abs(k_next - k_guess) < tolerance:
                return k_next
            k_guess = k_next

        return None

    k = find_wavenumber()
    print(k)
    
    def drag_inertia_force(z):
        u = H / 2 * omega * np.cosh(k * (z + d)) / np.sinh(k * d)
        acceleration = -omega**2 * H / 2 * np.cosh(k * (z + d)) / np.sinh(k * d)

        drag_force_z = 0.5 * rho * C_D * D * u**2
        inertia_force_z = rho * C_M * np.pi / 4 * D**2 * acceleration
        
        return drag_force_z, inertia_force_z
    
    drag_force_total = 0
    inertia_force_total = 0
    num_steps = 1000
    dz = d / num_steps  
    
    #Integrating over the water depth
    for i in range(num_steps):

        z = -d + i * dz
        
        drag_force_z, inertia_force_z = drag_inertia_force(z)

        drag_force_total += drag_force_z * dz
        inertia_force_total += inertia_force_z * dz
    
    F = drag_force_total + inertia_force_total

    z_moment = -d / 2 
    M_total = F * z_moment
    
    return F, M_total, inertia_force_total, drag_force_total


rho = 1025  # Density of seawater in kg/m³
C_M = 1.0   # Added mass coefficient
C_D = 0.7   # Drag coefficient
D = 8.0     # Diameter of the monopile
d = 60     # Water depth
g = 9.81    # Gravity
T = 8.76
H = 5.8

F_total, M_total, inertia, drag = morison_eq(rho, g, C_M, D, C_D, H, T, d)
print(f"Total Force: {F_total:.2f} N/m, Total Moment: {M_total:.2f} Nm, Inertia Force: {inertia:.2f} N/m, Drag Force: {drag:.2f} N/m")
if abs(inertia) > abs(drag):
    print('Inertia is the larger contribution.')
else:
    print('Drag is the largest contribution.')

